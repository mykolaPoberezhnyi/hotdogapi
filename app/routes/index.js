const hotdogsRoutes = require('./hotdogs_routes');
module.exports = function(app, db) {
  hotdogsRoutes(app, db);
};