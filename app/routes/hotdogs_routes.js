module.exports = function (app, db) {
    var ObjectID = require('mongodb').ObjectID;
    app.post('/hotdogs', (req, res) => {
        const hotdogs = { ...req.body };
        db.collection('hotdogs').insert(hotdogs, (err, result) => {
            if (err) {
                res.send({ 'error': 'An error has occurred' });
            } else {
                res.send(result.ops[0]);
            }
        });
    });

    app.get('/hotdogs/:id', (req, res) => {
        const id = req.params.id;
        const details = { '_id': new ObjectID(id) };
        db.collection('hotdogs').findOne(details, (err, item) => {
            if (err) {
                res.send({ 'error': 'An error has occurred' });
            } else {
                res.send(item);
            }
        });
    });

    app.get('/hotdogs', (req, res) => {
        db.collection('hotdogs').find({}).toArray((err, items) => {
            if (err) {
                res.send({ 'error': 'An error has occurred' });
            } else {
                res.send(items);
            }
        });
    });

    app.put('/hotdogs/:id', (req, res) => {
        const id = req.params.id;
        const details = { '_id': new ObjectID(id) };
        const note = { ...req.body };
        db.collection('hotdogs').update(details, note, (err, result) => {
            if (err) {
                res.send({ 'error': 'An error has occurred' });
            } else {
                res.send(note);
            }
        });
    });

    app.delete('/hotdogs/:id', (req, res) => {
        const id = req.params.id;
        const details = { '_id': new ObjectID(id) };
        db.collection('hotdogs').remove(details, (err, item) => {
            if (err) {
                res.send({ 'error': 'An error has occurred' });
            } else {
                res.send('Note ' + id + ' deleted!');
            }
        });
    });
};